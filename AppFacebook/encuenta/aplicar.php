<?php
include_once("../conector/functions.php");

$sqlQuery = new SQLQuery(); 
$line = $sqlQuery->getPYMENombreComercio($_POST["id"]);


$date = date('Y-m-d H:i:s');

$sql = "INSERT INTO respuesta
(Respuesta01,Respuesta02,Respuesta03,Respuesta04,Respuesta05,FechaRespuesta,GeneroID,RangoEdad,PymeID)
VALUES
(".$_POST['Respuesta01'].",".$_POST['Respuesta02'].",".$_POST['Respuesta03'].",".$_POST['Respuesta04'].",".$_POST['Respuesta05'].",
'".$date."','".$_POST['genero']."',".$_POST['RangoEdad'].",".$_POST['id'].")";
$result = $sqlQuery->getPYMEAplicar($sql);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $line['NombreComercio']; ?></title>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="right_col" role="main" style="margin-left: 0px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <img src="imagen.php?id=<?php echo $_POST['id']; ?>" style="width: 150px;" />
                <h3>La voz del cliente - <?php echo $line['NombreComercio']; ?></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Encuesta de satisfacci&oacute;n</h2>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">
                      <div class="alert alert-success" role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Exito:</span>
                          Tu encuesta fue enviada a nuestros colaboradores. ¡Muchas Gracias por su tiempo!
                        </div>
                  </div>
              </div>
            </div>
      </div>
    </div>
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>