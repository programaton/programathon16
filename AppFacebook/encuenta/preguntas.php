<?php
include_once("../config.php");
include_once("../conector/functions.php");
session_start();
//logica de Michael
//$_GET['id'] = 3;
$genero = "M";

$json = file_get_contents('./db.json', true);
$arrayJson = json_decode($json, true);

$sql = new SQLQuery();
//$line = $sql->getPYMENombreComercio($_GET["id"]);
$line = $sql->checkUserPYMELink($_SESSION['page_link']);
$line = $sql->getPYMENombreComercio($line["PymeID"]);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $line['NombreComercio']; ?></title>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="nav-md">
      
       <?php
       //echo $facebook->getUser();
       if(!$facebook->getUser()){
          header('Location: /AppFacebook/'); 
      }
      ?>
      
    <div class="container body">
      <div class="main_container">
        <div class="right_col" role="main" style="margin-left: 0px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <img src="imagen.php?id=<?php echo $_GET['id']; ?>" style="width: 150px;" />
                <h3>La voz del cliente - <?php echo $line['NombreComercio']; ?></h3>
                 <?php
                 //echo '<div class="centro"><a href="../logout.php?logout=1&id='.$_GET["id"].'"><img src="../images/logout.jpg"></a><div>'; 
                  ?>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Encuesta de satisfacci&oacute;n</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="formEncuesta" data-parsley-validate class="form-horizontal form-label-left" action="aplicar.php" method="post">
                        <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
                        <input type="hidden" name="genero" id="genero" value="<?php echo $genero; ?>" />
                        <label>&#191;<?php echo $arrayJson['pregunta01']; ?>&#63;</label>
                        <label id="msgpregunta01" class="hidden red">Pregunta es requerida</label>
                        <ul class="nav">
                            <li><input type="radio" class="flat" name="Respuesta01" id="Respuesta01Malo" value="1" /> Malo</li>
                            <li><input type="radio" class="flat" name="Respuesta01" id="Respuesta01Regular" value="2" /> Regular</li>
                            <li><input type="radio" class="flat" name="Respuesta01" id="Respuesta01Normal" value="3" /> Normal</li>
                            <li><input type="radio" class="flat" name="Respuesta01" id="Respuesta01Bueno" value="4" /> Bueno(a)</li>
                            <li><input type="radio" class="flat" name="Respuesta01" id="Respuesta01Excelente" value="5" /> Excelente</li>
                        </ul>
                        <br />
                        <label>&#191;<?php echo $arrayJson['pregunta02']; ?>&#63;</label>
                        <label id="msgpregunta02" class="hidden red">Pregunta es requerida</label>
                        <ul class="nav">
                            <li><input type="radio" class="flat" name="Respuesta02" id="Respuesta02Malo" value="1" /> Malo</li>
                            <li><input type="radio" class="flat" name="Respuesta02" id="Respuesta02Regular" value="2" /> Regular</li>
                            <li><input type="radio" class="flat" name="Respuesta02" id="Respuesta02Normal" value="3" /> Normal</li>
                            <li><input type="radio" class="flat" name="Respuesta02" id="Respuesta02Bueno" value="4" /> Bueno(a)</li>
                            <li><input type="radio" class="flat" name="Respuesta02" id="Respuesta02Excelente" value="5" /> Excelente</li>
                        </ul>
                        <br />
                        <label>&#191;<?php echo $arrayJson['pregunta03']; ?>&#63;</label>
                        <label id="msgpregunta03" class="hidden red">Pregunta es requerida</label>
                        <ul class="nav">
                            <li><input type="radio" class="flat" name="Respuesta03" id="Respuesta03Malo" value="1" /> Malo</li>
                            <li><input type="radio" class="flat" name="Respuesta03" id="Respuesta03Regular" value="2" /> Regular</li>
                            <li><input type="radio" class="flat" name="Respuesta03" id="Respuesta03Normal" value="3" /> Normal</li>
                            <li><input type="radio" class="flat" name="Respuesta03" id="Respuesta03Bueno" value="4" /> Bueno(a)</li>
                            <li><input type="radio" class="flat" name="Respuesta03" id="Respuesta03Excelente" value="5" /> Excelente</li>
                        </ul>
                        <br />
                        <label>&#191;<?php echo $arrayJson['pregunta04']; ?>&#63;</label>
                        <label id="msgpregunta04" class="hidden red">Pregunta es requerida</label>
                        <ul class="nav">
                            <li><input type="radio" class="flat" name="Respuesta04" id="Respuesta04Malo" value="1" /> Malo</li>
                            <li><input type="radio" class="flat" name="Respuesta04" id="Respuesta04Regular" value="2" /> Regular</li>
                            <li><input type="radio" class="flat" name="Respuesta04" id="Respuesta04Normal" value="3" /> Normal</li>
                            <li><input type="radio" class="flat" name="Respuesta04" id="Respuesta04Bueno" value="4" /> Bueno(a)</li>
                            <li><input type="radio" class="flat" name="Respuesta04" id="Respuesta04Excelente" value="5" /> Excelente</li>
                        </ul>
                        <br />
                        <label>&#191;<?php echo $arrayJson['pregunta05']; ?>&#63;</label>
                        <label id="msgpregunta05" class="hidden red">Pregunta es requerida</label>
                        <ul class="nav">
                            <li><input type="radio" class="flat" name="Respuesta05" id="Respuesta05Malo" value="1" /> Malo</li>
                            <li><input type="radio" class="flat" name="Respuesta05" id="Respuesta05Regular" value="2" /> Regular</li>
                            <li><input type="radio" class="flat" name="Respuesta05" id="Respuesta05Normal" value="3" /> Normal</li>
                            <li><input type="radio" class="flat" name="Respuesta05" id="Respuesta05Bueno" value="4" /> Bueno(a)</li>
                            <li><input type="radio" class="flat" name="Respuesta05" id="Respuesta05Excelente" value="5" /> Excelente</li>
                        </ul>
                        <br />
                        <label for="heard">&#191;<?php echo $arrayJson['pregunta06']; ?>&#63;</label>
                          <select id="RangoEdad" name="RangoEdad" class="form-control" required style="max-width: 150px">
                            <option value="1">12-17</option>
                            <option value="2">18-33</option>
                            <option value="3">34-45</option>
                            <option value="4">46-55</option>
                            <option value="5">56-64</option>
                            <option value="6">65-73</option>
                            <option value="7">74+</option>
                          </select>
                          <div class="ln_solid"></div>
                          <button type="submit" class="btn btn-success"><?php echo $arrayJson['boton']; ?></button>
                          <div class="alert alert-danger hidden" role="alert" id="general">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                          Todas las preguntas son requeridas
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <script src="js/moment/moment.min.js"></script>
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <script src="../build/js/custom.min.js"></script>
          <script>
              $(document).ready(function () {
                  $('#formEncuesta').on('submit', function () {
                      var band = true;
                      if ($('input[name="Respuesta01"]').is(':checked')) {
                          $('#msgpregunta01').addClass("hidden");
                      } else {
                          $('#msgpregunta01').removeClass("hidden");
                          band = false;
                      }
                      if ($('input[name="Respuesta02"]').is(':checked')) {
                          $('#msgpregunta02').addClass("hidden");
                      } else {
                          $('#msgpregunta02').removeClass("hidden");
                          band = false;
                      }
                      if ($('input[name="Respuesta03"]').is(':checked')) {
                          $('#msgpregunta03').addClass("hidden");
                      } else {
                          $('#msgpregunta03').removeClass("hidden");
                          band = false;
                      }
                      if ($('input[name="Respuesta04"]').is(':checked')) {
                          $('#msgpregunta04').addClass("hidden");
                      } else {
                          $('#msgpregunta04').removeClass("hidden");
                          band = false;
                      }
                      if ($('input[name="Respuesta05"]').is(':checked')) {
                          $('#msgpregunta05').addClass("hidden");
                      } else {
                          $('#msgpregunta05').removeClass("hidden");
                          band = false;
                      }
                      if (!band) {
                          $('#general').removeClass("hidden");
                      } else {
                          $('#general').addClass("hidden");
                      }
                      return band;
                  });
              });
          </script>
  </body>
</html>
