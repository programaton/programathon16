<?php
class SQLQuery { 
	function __construct(){
            //database configuration
            $dbServer = 'localhost'; //Define database server host
            $dbUsername = 'root'; //Define database username
            $dbPassword = ''; //Define database password
            $dbName = 'programathon2016'; //Define database name

            //connect databse
            $con = mysqli_connect($dbServer,$dbUsername,$dbPassword,$dbName);
            if(mysqli_connect_errno()){
                die("Error al conectar con MySQL: ".mysqli_connect_error());
            }else{
                $this->connect = $con;
            } 
	}
 
        function checkUserPYME($id){ 
		    $query = mysqli_query($this->connect,"SELECT * FROM pyme WHERE  Id = '".$id."'") or die(mysql_error($this->connect));
		    $result = mysqli_fetch_array($query);
		    return $result;
	    }
        
        function checkUserPYMELink($link){ 
            $link = str_replace("\\","/",$link);
            $ultimo = substr($link, -1);
            if($ultimo){
                $link = substr($link, 0, -1);
            }
            //echo "SELECT * FROM redsocial WHERE InformacionContacto like '%".$link."%' limit 1";
            //$query = mysqli_query($this->connect,"SELECT * FROM redsocial WHERE lower(Link) like lower('".$link."%') limit 1") or die(mysql_error($this->connect));
            $query = mysqli_query($this->connect,"SELECT * FROM redsocial WHERE InformacionContacto like '%".$link."%' limit 1") or die(mysql_error($this->connect));
            $result = mysqli_fetch_array($query);
            //print_r($result);
            return $result;
        }
        
        function checkUserPYMEExist($id){  
                $query = mysqli_query($this->connect,"SELECT Id FROM pyme WHERE  Id = '".$id."'") or die(mysql_error($this->connect));
                if(mysqli_num_rows($query)>0){
                    $result = 1;
                }else{
                    $result = 0;
                }
                return  $result;  
	}
        
         function getPYMENombreComercio($id){    
             $query = mysqli_query($this->connect,"SELECT NombreComercio FROM pyme WHERE  Id = '".$id."'") or die(mysql_error($this->connect));
             $result = mysqli_fetch_array($query); 
              return $result; 
         }
         
         
         function getPYMEAplicar($sql){ 
            $query = mysqli_query($this->connect,$sql) or die(mysql_error($this->connect));
            $result = mysqli_fetch_array($query); 
            return $result; 
         }

         function getPYMEGraficos($id,$pregunta,$fechaInicio,$fechaFin,&$valores){    
             ini_set('display_errors', 1);
             ini_set('display_startup_errors', 1);
             error_reporting(E_ALL);
			$sql1 = "select count(1) total from respuesta where PymeID = '".$id."' and FechaRespuesta BETWEEN STR_TO_DATE('".$fechaInicio." 01:00:00','%d/%m/%Y %T') AND STR_TO_DATE('".$fechaFin." 23:59:59','%d/%m/%Y %T')";
            $query = mysqli_query($this->connect,$sql1) or die(mysql_error($this->connect));
            $result = mysqli_fetch_array($query,MYSQLI_ASSOC);
            $total = $result['total'];
			//echo $sql1."<br>";
            
            $valores[1] = 0;
            $valores[2] = 0;
            $valores[3] = 0;
            $valores[4] = 0;
            $valores[5] = 0;
            $sql1 = "SELECT Respuesta0".$pregunta." i,count(1) valor FROM respuesta where PymeID = '".$id."' and FechaRespuesta BETWEEN STR_TO_DATE('".$fechaInicio." 01:00:00','%d/%m/%Y %T') AND STR_TO_DATE('".$fechaFin." 23:59:59','%d/%m/%Y %T') group by Respuesta0".$pregunta;
            $query = mysqli_query($this->connect,$sql1) or die(mysql_error($this->connect));
//echo $sql1."<br>";
            while ($fila = mysqli_fetch_array($query, MYSQLI_ASSOC )) {
                $valores[$fila['i']] = number_format(($fila['valor']*100 / $total), 2, '.', '');
            }
            $data = "[".$valores[1].", ".$valores[2].", ".$valores[3].", ".$valores[4].", ".$valores[5]."]";
            return $data; 
         }
         
         function getPYMEGraficosEdad($id,$fechaInicio,$fechaFin){ 
             $sql1 = "SELECT  RangoEdad i , count(1)  valor from respuesta
                where PymeID = '".$id."' and FechaRespuesta BETWEEN STR_TO_DATE('".$fechaInicio." 01:00:00','%d/%m/%Y %T') AND STR_TO_DATE('".$fechaFin." 23:59:59','%d/%m/%Y %T') group by RangoEdad";
             $query = mysqli_query($this->connect,$sql1) or die(mysql_error($this->connect));
             
             $valores[1] = 0;
             $valores[2] = 0;
             $valores[3] = 0;
             $valores[4] = 0;
             $valores[5] = 0;
             $valores[6] = 0;
             $valores[7] = 0; 
             
             //echo "<br><br><br><br><br><br><br><br><br>".$sql1;
             
             while ($fila = mysqli_fetch_array($query, MYSQLI_ASSOC )) {
                 $valores[$fila['i']] = number_format($fila['valor'], 0, '.', '');
             }
             $data = "[".$valores[1].", ".$valores[2].", ".$valores[3].", ".$valores[4].", ".$valores[5].$valores[6].", ".$valores[7]."]";
             return $data; 
             
         }
		 function getPYMEGraficosGenero($id,$fechaInicio,$fechaFin){ 
             $sql1 = "SELECT  GeneroID i , count(1)  valor from respuesta
                where PymeID = '".$id."' and FechaRespuesta BETWEEN STR_TO_DATE('".$fechaInicio." 01:00:00','%d/%m/%Y %T') AND STR_TO_DATE('".$fechaFin." 23:59:59','%d/%m/%Y %T') group by GeneroID order by GeneroID";
             $query = mysqli_query($this->connect,$sql1) or die(mysql_error($this->connect));
             
			 //echo "<br><br><br><br><br><br><br><br><br>".$sql1;
             $valores['M'] = 0;
             $valores['F'] = 0;
             $valores['N'] = 0;
             
             //echo "<br><br><br><br><br><br><br><br><br>".$sql1;
             
             while ($fila = mysqli_fetch_array($query, MYSQLI_ASSOC )) {
                 $valores[$fila['i']] = number_format($fila['valor'], 0, '.', '');
             }
             $data = "[".$valores['F'].", ".$valores['M'].", ".$valores['N']."]";
             return $data; 
             
         }
         

}
?>