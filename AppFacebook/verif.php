<?php
session_start();
require_once("./php-graph-sdk-5.0.0/src/Facebook/autoload.php");
$fb = new Facebook\Facebook([
  'app_id' => '254767638255889',
  'app_secret' => 'e64f290bbf5b532f6498d7a905c2e3ae'
]);

$helper = $fb->getRedirectLoginHelper();
try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (isset($accessToken)) {
  // Logged in!
  $_SESSION['facebook_access_token'] = (string) $accessToken;

  // Now you can redirect to another page and use the
  // access token from $_SESSION['facebook_access_token']
}

header('Location: usuario.php');
exit;
?>