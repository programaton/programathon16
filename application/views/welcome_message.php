<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('header.php'); ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
               

              <h1 class="error-number"><i class="fa fa-thumbs-o-up"></i> Programathon 2016!</h1>
              <h1>La Voz Del Cliente</h1>
              <p>

“La Voz del Cliente” es un sistema para que las PYMES asociadas a FUNDES reciban <br>
retroalimentación de sus clientes finales y a su vez, usar esta información como insumo para<br>
desarrollar planes de mejoramiento que conlleven a la mejora continua y aumento de la competitividad.

              </p>
              <div class="mid_center">
                <form>
                   
                    <button onclick="javascript:location.href='<?php echo base_url("/login"); ?>'" type="button" class="btn btn-success btn-lg"> Ingresar </button>

                    <button onclick="javascript:location.href='<?php echo base_url("/pymeRegistrar"); ?>'" type="button" class="btn btn-info btn-lg">Registrarse</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>


  <?php $this->load->view('scripts.php'); ?>

  </body>
</html>
