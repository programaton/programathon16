<?php $attributes = array('id' => 'form_login'); 

    $username = array('name'        =>'username'
                     ,'id'          =>'username'
                     ,'class'       =>'form-control'
                     ,'placeholder' =>'Nombre de Usuario' 
                     ,'required'   =>'required'
                     ,'value'      => set_value('username')
                     ,'maxlength'   => '50'
                     ,'size'       => '35'
                     ,);

    $password = array('name'        =>'password'
                     ,'id'          =>'password'
                     ,'class'       =>'form-control'
                     ,'placeholder' =>'Contraseña'
                     , 'required'   =>'required'
                     ,'type'        =>'password'
                     ,'size'        =>'35'
                     ,'maxlength'   => '10'
                     ,'minlength'   => '8'
                     ,);

    $pyme = array('name'        => 'pymename'
                 ,'id'          => 'pymename'
                 ,'class'       => 'form-control'
                 ,'placeholder' => 'Nombre Comercial'
                 ,'required'    => 'required'
                 ,'value'       => set_value('pymename')
                 ,'size'        => '35'
                 ,'maxlength'   => '100'
                 ,);

 ?> 


<?php $this->load->view('header.php'); ?>



  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">

<?php if(validation_errors()):       
 ?> 

<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                     </button>
  <div id="error"><?=validation_errors();?></div>
                  </div>


<?php endif ?>



<h2>Login Form</h2>
<?=form_open('login', $attributes);?>    
              

<div id='msg_pyme' class="padding"></div> 
<div ><?=form_input($pyme)?></div>

<div id='msg_username' class="padding"></div> 
<div ><?=form_input($username);?></div>

<div id='msg_password' class="padding"></div> 
<div ><?=form_input($password)?></div>

<div ><?=form_label('Pais')?></div>
<div id='msg_pais' class="padding"></div> 
<div ><?=form_dropdown('pais', $paises, '0', 'class="form-control" ')?></div>
<br><br>
<?=form_submit(array('name' => 'submit','class'=>'btn btn-default submit',
                                               'value' => 'Aceptar'))?>

<?=form_reset(array('name' => 'cancel','class'=>'btn btn-danger',
                                               'value' => 'Cancelar'))?>
<?=form_close();?>


              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Nuevo en el sitio?
                  <a class="to_register" href="<?php echo base_url("/pymeRegistrar"); ?>">Registrarse</a>
                </p>

                <p class="change_link">Olvido su Contraseña?
                  <a class="to_register" href="mailto:example@gmail.com">Recuperar</a>
                </p>


                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-thumbs-o-up"></i> Programathon 2016!</h1>
                  <p>©2016 All Rights Reserved. Fiserv. Fundes. GetCode().</p>
                </div>
              </div>
            </form>
          </section>
        </div>


      </div>
    </div>


        <!-- footer content -->
        <?php $this->load->view('scripts.php'); ?>

  </body>
</html>



