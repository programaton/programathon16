<?php if ( isset($Registrar) && TRUE == $Registrar): ?>
<?php $this->load->view('header.php'); ?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>La Voz Del Cliente</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>RedSocial <?php echo $button ?></h2>
                    
                    <!-- toolbox -->
                     <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 
        <form action="<?php echo $action; ?>" method="post">
        <?php endif; ?>


        <?php 

        for ($i=1; $i < 6; $i++) { ?>

      <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Direccion <?php echo $TipoRedSocialID[$i] ;?> </label>
            <div class="col-md-6 col-sm-6 col-xs-12">

    <?php if(isset($valor)){ ?>
    
    <input type="text" class="form-control" name="valor[]" id="NombreCompleto" placeholder="" value="<?php echo $valor[$i] ;?>"  maxlength="50" required  />

    <?php }else{ ?> 

            <input type="text" class="form-control" name="valor[]" <?php if($i == 1){?> required <?php } ?>"  id="NombreCompleto" placeholder="Direccion <?php echo $TipoRedSocialID[$i] ;?>"  maxlength="300"  />
   
    <?php } ?>        

            <input type="hidden" class="form-control" name="id[]" id="" placeholder="Link" value="<?php echo $i; ?>" />

            </div>
        </div>


      <?php } ?>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Correo electrónico <?php echo form_error('InformacionContacto') ?></label>

    
            <div class="col-md-6 col-sm-6 col-xs-12">

            <input type="email" maxlength="50" class="form-control" name="InformacionContacto" id="InformacionContacto" placeholder="InformacionContacto" value="<?php if(isset($InformacionContacto)){ echo $InformacionContacto; } ?>" />


            </div>

        </div>




        <?php if ( isset($Registrar) && TRUE == $Registrar): ?>
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('PymeRedsocial') ?>" class="btn btn-default">Cancel</a>
       </form>
     

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>
        <?php endif;?>