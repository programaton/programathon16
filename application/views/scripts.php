    <!-- jQuery -->
    <script src="<?php echo base_url("assets/vendors/jquery/dist/jquery.min.js"); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url("assets/vendors/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url("assets/vendors/fastclick/lib/fastclick.js"); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url("assets/vendors/nprogress/nprogress.js"); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url("assets/js/validate_user.js"); ?>"></script>

    <!-- jQuery Smart Wizard -->
    <script src="<?php echo base_url("assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"); ?>"></script>


    <?php if(isset($js_files)){ ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <?php } ?>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url("assets/build/js/custom.min.js"); ?>"></script>


    
    <!-- jQuery Smart Wizard -->
    <script>
       jQuery(document).ready(function() {

            //binds to onchange event of your input field
            $('#Logo').bind('change', function() {

              //this.files[0].size gets the size of your file.
               var fsize = this.files[0].size;

                        if(fsize>50000) //do something if file size more than 1 mb (1048576)
                        {
                            $("#Logo").val(""); 
                            alert('La imagen es muy grande, por favor elija otra.');
                        }

            });


        });
    </script>

    <!-- /jQuery Smart Wizard -->


