<?php $this->load->view('header.php'); ?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>La Voz Del Cliente</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Usuario Lista</h2>
                    
                    <!-- toolbox -->
                    <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->
   

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  


        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('pymeUsuario/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('pymeUsuario/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('pymeUsuario'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
        <th>Usuario</th>
        <th>NombreCompleto</th>
        <th>Clave</th>
        <th>EmailContacto</th>
        <th>Action</th>
            </tr><?php
            foreach ($pymeusuario_data as $pymeusuario)
            {
                ?>
                <tr>
            <td width="80px"><?php echo ++$start ?></td>
            <td><?php echo $pymeusuario->Usuario ?></td>
            <td><?php echo $pymeusuario->NombreCompleto ?></td>
            <td><?php echo $pymeusuario->Clave ?></td>
            <td><?php echo $pymeusuario->EmailContacto ?></td>
            <td style="text-align:center" width="200px">
                <?php 
                echo anchor(site_url('pymeUsuario/read/'.$pymeusuario->ID),'Read'); 
                echo ' | '; 
                echo anchor(site_url('pymeUsuario/update/'.$pymeusuario->ID),'Update'); 
                echo ' | '; 
                echo anchor(site_url('pymeUsuario/delete/'.$pymeusuario->ID),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>






                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>
