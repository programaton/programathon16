<?php if ( isset($Registrar) && TRUE == $Registrar): ?>
<?php $this->load->view('header.php'); ?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>La Voz Del Cliente</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Usuario <?php echo $button ?></h2>
                    
                    <!-- toolbox -->
                    <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->
   

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  

        <form action="<?php echo $action; ?>" method="post">
        <?php endif;?>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Usuario <?php echo form_error('Usuario') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="Usuario" id="Usuario" placeholder="Usuario" value="<?php echo $Usuario; ?>"  maxlength="50" required  />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Nombre Completo <?php echo form_error('NombreCompleto') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">

            <input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" placeholder="NombreCompleto" value="<?php echo $NombreCompleto; ?>"  maxlength="50" required  />

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Contraseña <?php echo form_error('Clave') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" class="form-control" name="Clave" id="Clave" placeholder="Contraseña" value="<?php echo $Clave; ?>"  maxlength="10" minlength="10"  required />
            </div>
        </div>
        
         <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Repetir Contraseña <?php echo form_error('Clave') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" class="form-control" name="ClaveRepetir" id="ClaveRepetir" placeholder="Repetir Contraseña" value="<?php echo $Clave; ?>" maxlength="50" required />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Correo  Electrónico <?php echo form_error('EmailContacto') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="email" class="form-control" name="EmailContacto" id="EmailContacto" placeholder="Correo  Electrónico" value="<?php echo $EmailContacto; ?>"  maxlength="50" required />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="varchar">Repetir Correo  Electrónico <?php echo form_error('RepetirEmailContacto') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="email" class="form-control" name="RepetirEmailContacto" id="RepetirEmailContacto" placeholder="Repetir Correo  Electrónico" value="<?php echo $EmailContacto; ?>" />
            </div>
        </div>


        




        <input type="hidden" name="ID" value="<?php echo $ID; ?>" /> 
        <?php if ( isset($Registrar) && TRUE == $Registrar): ?>
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('pymeUsuario') ?>" class="btn btn-default">Cancel</a>
    </form>





                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>
        <?php endif; ?>