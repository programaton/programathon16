<?php $this->load->view('header.php'); ?>
<?php $Pyme_id = $this->session->userdata('Pyme_id'); ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><img src="http://localhost/programathon16/AppFacebook/encuenta/imagen.php?id=<?php echo $Pyme_id; ?>" style="width: 70px;" /></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title . ' - ' . $pyme;?></h2>
                     <!-- toolbox -->
                    <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->
   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  

                  <?php echo $output; ?>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>