<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('header.php'); ?>

  <body class="login">
    <div class="">
      <div class="">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
               

              <h1 class="error-number"><i class="fa fa-thumbs-o-up"></i> Programathon 2016!</h1>
              <h1>La Voz Del Cliente</h1>




          <form method="POST" action="<?php echo $action; ?>" class="form-horizontal form-label-left">

          <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Seccion 1<br />
                                              <small>Informacion PYME</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Seccion 2<br />
                                              <small>Informacion Redes Sociales</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Seccion 3<br />
                                              <small>Informacion Usuario</small>
                                          </span>
                          </a>
                        </li>
                      </ul>

              
                      <div id="step-1">


                        <?php echo $form_pyme; ?>


                      </div>

                      <div class="separator"></div>
                      <div id="step-2">
                        <h2 class="StepTitle">Seccion 2 - Informacion Redes Sociales</h2>
                      
                        <?php echo $form_red; ?>

                      </div>
                      <div class="separator"></div>
                      
                      <div id="step-3">
                        <h2 class="StepTitle">Seccion 3 - Informacion Usuario</h2>
                      
                        <?php echo $form_user; ?>

                      </div>


                      <input type="submit">Guardar</input>

        
            


                    </div>
                    <!-- End SmartWizard Content -->

              </form>



              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>


  <?php $this->load->view('scripts.php'); ?>

  </body>
</html>
