<?php $this->load->view('header.php'); ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>La Voz Del Cliente</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>RedSocial <?php echo $button ?></h2>
                    
                    <!-- toolbox -->
                     <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


			        <table class="table">
				    <tr><td>NombreComercio</td><td><?php echo $NombreComercio; ?></td></tr>
				    <tr><td>EstadoID</td><td><?php echo $EstadoID; ?></td></tr>
				    <tr><td>SectorID</td><td><?php echo $SectorID; ?></td></tr>
				    <tr><td>AnnoInicioOperaciones</td><td><?php echo $AnnoInicioOperaciones; ?></td></tr>
				    <tr><td>NumeroTelefono</td><td><?php echo $NumeroTelefono; ?></td></tr>
				    <tr><td>Direccion</td><td><?php echo $Direccion; ?></td></tr>
				    <tr><td>EsActiva</td><td><?php echo $EsActiva; ?></td></tr>
				    <tr><td>EsNegocioFamiliar</td><td><?php echo $EsNegocioFamiliar; ?></td></tr>
				    <tr><td>Logo</td><td><?php echo $Logo; ?></td></tr>
				    <tr><td>ExtensionLogo</td><td><?php echo $ExtensionLogo; ?></td></tr>
				    <tr><td>FechaCreacion</td><td><?php echo $FechaCreacion; ?></td></tr>
				    <tr><td>FechaUltimaActualizacion</td><td><?php echo $FechaUltimaActualizacion; ?></td></tr>
				    <tr><td>EsFacebookAppInstalado</td><td><?php echo $EsFacebookAppInstalado; ?></td></tr>
				    <tr><td>UsuarioID</td><td><?php echo $UsuarioID; ?></td></tr>
				    <tr><td>GeneroPropietarioID</td><td><?php echo $GeneroPropietarioID; ?></td></tr>
				    <tr><td>CedJuridica</td><td><?php echo $CedJuridica; ?></td></tr>
				    <tr><td></td><td><a href="<?php echo site_url('pyme') ?>" class="btn btn-default">Cancel</a></td></tr>
					</table>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>

