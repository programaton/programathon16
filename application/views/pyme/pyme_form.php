<?php if ( isset($Registrar) && TRUE == $Registrar): ?>
<?php $this->load->view('header.php'); ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>La Voz Del Cliente</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->load->view('menu-profile.php'); ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('menu-footer-buttons.php'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
         <?php $this->load->view('top-navigation.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>RedSocial <?php echo $button ?></h2>
                    
                    <!-- toolbox -->
                     <?php $this->load->view('toolbox.php'); ?>
                    <!-- /toolbox -->

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



        <form action="<?php echo $action; ?>" method="post">

        <?php endif; ?>
      
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="char">Nombre Comercial <?php echo form_error('NombreComercio') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="NombreComercio" id="NombreComercio" placeholder="NombreComercio" value="<?php echo $NombreComercio; ?>" <?php if( !empty($NombreComercio)){ ?> disabled="disabled" <?php }?> maxlength="100" required />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="int">País</label>

            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_dropdown('PaisID', $PaisID, (isset($u_paisID) ? $u_paisID : 0), 'class="form-control" ');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="int">Provincia/Estado/Departamento <?php echo form_error('Estado') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_dropdown('EstadoID', $EstadoID, (isset($u_estadoID) ? $u_estadoID : 0),'class="form-control" ');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="int">Sector <?php echo form_error('SectorID') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_dropdown('SectorID', $SectorID, (isset($u_sectorID) ? $u_sectorID : 0),'class="form-control" ');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="tinyint">Año de inicio de operaciones <?php echo form_error('AnnoInicioOperaciones') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_dropdown('AnnoInicioOperaciones', $AnnoInicioOperaciones, (isset($u_annoInicioOperaciones) ? $u_annoInicioOperaciones : 0),'class="form-control" ');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="char">Teléfono <?php echo form_error('NumeroTelefono') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="NumeroTelefono" id="NumeroTelefono" placeholder="NumeroTelefono" value="<?php echo $NumeroTelefono; ?>" maxlength="50" required />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="char">Direccion <?php echo form_error('Direccion') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="Direccion" id="Direccion" placeholder="Direccion" value="<?php echo $Direccion; ?>" maxlength="200" required />
            </div>
        </div>
        <div class="form-group">
            <?php if ($isCreate == 0):?>
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="bit">PYME activa? <?php echo form_error('EsActiva') ?></label>
                <div class="col-md-1 col-sm-6 col-xs-12">
                <input type="checkbox" name="EsActiva" value="text1" class="form-control" <?php echo ($EsActiva == 1 ? 'checked' : null); ?>>
                </div>
            <?php endif;?>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="bit">Negocio familiar?</label>
            <div class="col-md-1 col-sm-6 col-xs-12">
            <input type="checkbox" name="EsNegocioFamiliar" class="form-control"  value="text1" <?php echo ($EsNegocioFamiliar == 1 ? 'checked' : 'unchecked'); ?>>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="blob">Adjuntar logo <?php echo form_error('Logo') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" class="form-control" name="Logo" id="Logo" accept="image/*" placeholder="Logo" value="<?php echo $Logo; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="datetime"><?php echo form_error('FechaUltimaActualizacion') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" class="form-control" name="FechaUltimaActualizacion" id="FechaUltimaActualizacion" placeholder="FechaUltimaActualizacion" value="<?php echo date(' Y-m-d h:i:s '); ?>" />
            </div>
        </div>
        <!--<div class="form-group">
            <label for="bit">EsFacebookAppInstalado <?php //echo form_error('EsFacebookAppInstalado') ?></label>
            <input type="text" class="form-control" name="EsFacebookAppInstalado" id="EsFacebookAppInstalado" placeholder="EsFacebookAppInstalado" value="<?php //echo $EsFacebookAppInstalado; ?>" />
        </div>-->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" class="form-control" name="UsuarioID" id="UsuarioID" placeholder="UsuarioID" value="<?php echo $UsuarioID; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="varchar">GeneroPropietarioID <?php echo form_error('GeneroPropietarioID') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_dropdown('GeneroPropietarioID', $GeneroPropietarioID, (isset($u_generoPropietarioID) ? $u_generoPropietarioID : 4),'class="form-control" ');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" for="varchar">CedJuridica <?php echo form_error('CedJuridica') ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="CedJuridica" id="CedJuridica" placeholder="CedJuridica" value="<?php echo $CedJuridica; ?>" maxlength="50" required/>
            </div>
        </div>
        <input type="hidden" name="Id" value="<?php echo $Id; ?>" /> 

        <?php if ( isset($Registrar) && TRUE == $Registrar): ?>
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('pyme') ?>" class="btn btn-default">Cancel</a>
    </form>






                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <?php $this->load->view('footer.php'); ?>
        <?php endif;?>

