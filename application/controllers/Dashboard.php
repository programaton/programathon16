<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{
   
   function __construct() 
   { 
        parent::__construct();
        $this->load->model('Pyme_model');
        $this->load->library(array('session','form_validation'));
        $this->load->view('admin-zone');
        $this->load->database();



   }
  
	function index()
	{	

		$data['title'] 		 = 'Panel de Métricas';
		$data['pyme']  		 =  $this->session->userdata('Pyme');
		$data['pymeId']  		 =  $this->session->userdata('Pyme_id');
		$data['user']  		 =  $this->session->userdata('user');
		$data['output']		 = 'MAINCONTENT';
		$data['fechaInicio'] = $this->input->post('fechaInicio');
		$data['fechaFin']	 = $this->input->post('fechaFin');
		$data['appInstall']	 =  $this->Pyme_model->get_flag_install($this->session->userdata('Pyme_id'));
		//print_r($this->session->userdata());
		//$json = file_get_contents($this->base().'db$pyme.json', true);
		//$arrayJson = json_decode($json, true);
		$this->get_metricas();

		$this -> form_validation -> set_error_delimiters('<ul><li>', '</li></ul>');

		if ($this->form_validation->run() == FALSE)
        {
			$this->load->view('dashboard', $data);
		}
	}


	function get_metricas()
	{
		// reglas de validación
        $this->form_validation->set_rules('finicial', 'Finicial', 'callback__valid_finicial');
       	$this->form_validation->set_rules('ffinal', 'Ffinal', 'callback__valid_final');

       	$this->form_validation->set_message('_valid_finicial', 'Verifique que su Fecha Inicial no sea mayor al dia de hoy o no supere la fecha final');
        $this->form_validation->set_message('_valid_final', 'Verifique que su Fecha Final no sea menor al dia de hoy o no sea menor que la fecha inicial');

	}


	function _valid_finicial()
	{
		$finicial = $this->input->post('finicial');
        $ffinal   = $this->input->post('ffinal');
        $status   = TRUE;

        if($finicial > $ffinal || $finicial > date("Y-m-d"))
        {
			$status	= FALSE;
        }

        return $status;

	}

	function _valid_final()
	{
		$finicial = $this->input->post('finicial');
        $ffinal   = $this->input->post('ffinal');
        $status   = TRUE;

        if($finicial < $ffinal || $ffinal < date("Y-m-d"))
        {
			$status	= FALSE;
        }

        return $status;
	}
}

