<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PymeRegistrar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();

        $this->load->model('Pyme_model');
        $this->load->model('PymeUsuario_model');
        $this->load->model('PymeRedsocial_model');

		$this->load->library(array('session','form_validation'));   
	}


	public function index(){

		$data['title'] = 'Registrar Pyme';
		$data['pyme']  =  $this->session->userdata('Pyme');
		$data['user']  =  $this->session->userdata('user');


        $data_pyme = array(
            'isCreate' => '1',
            'button' => 'Create',
            'action' => site_url('pyme/create_action'),
        'Id' => set_value('Id'),
        'NombreComercio' => set_value('NombreComercio'),
        'PaisID' => $this->Pyme_model->get_pais(),
        'EstadoID' => $this->Pyme_model->get_all_states(),
        'SectorID' => $this->Pyme_model->get_sectors(),
        'AnnoInicioOperaciones' => $this->Pyme_model->get_creation_year(),
        'NumeroTelefono' => set_value('NumeroTelefono'),
        'Direccion' => set_value('Direccion'),
        'EsActiva' => set_value('EsActiva'),
        'EsNegocioFamiliar' => set_value('EsNegocioFamiliar'),
        'Logo' => set_value('Logo'),
        'ExtensionLogo' => set_value('ExtensionLogo'),
        'FechaCreacion' => set_value('FechaCreacion'),
        'FechaUltimaActualizacion' => set_value('FechaUltimaActualizacion'),
        'EsFacebookAppInstalado' => set_value('EsFacebookAppInstalado'),
        'UsuarioID' => set_value('UsuarioID'),
        'GeneroPropietarioID' => $this->Pyme_model->get_gender(),
        'CedJuridica' => set_value('CedJuridica'),
    );
        


        $data_red = array(
            'button' => 'Create',
            'action' => site_url('pymeRedsocial/create_action'),
        'TipoRedSocialID' => set_value('TipoRedSocialID'),
        'Comentario' => set_value('Comentario'),
        'InformacionContacto' => set_value('InformacionContacto'),
        'PymeID' => set_value('PymeID'),
        'Link' => set_value('Link'),
        'TipoRedSocialID' => $this->PymeRedsocial_model->get_red_social_type(),
         );    



        $data_user = array(
            'button' => 'Create',
            'action' => site_url('pymeusuario/create_action'),
	    'ID' => set_value('ID'),
	    'Usuario' => set_value('Usuario'),
	    'NombreCompleto' => set_value('NombreCompleto'),
	    'Clave' => set_value('Clave'),
	    'EmailContacto' => set_value('EmailContacto'),
		);


		$data['form_pyme']	= $this->load->view('pyme/pyme_form', $data_pyme, TRUE);

		$data['form_red']	= $this->load->view('pymeredsocial/redsocial_form', $data_red,TRUE);

		$data['form_user']	= $this->load->view('pymeusuario/usuario_form', $data_user, TRUE);

        $data['action']     =   site_url('pymeRegistrar/create_action');
        
		$this->load->view('pymeregistrar',$data);

	}


    public function create_action() 
    {   


        print_r($this->input->post());

        $path = $this->input->post('Logo',TRUE);
        $ext  = '';

        if( !empty($path) )
        {
            $ext = pathinfo($path, PATHINFO_EXTENSION);
        }
        
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            ///////////////////////////////////////////////////////

            $data_user = array(
                'Usuario' => $this->input->post('Usuario',TRUE),
                'NombreCompleto' => $this->input->post('NombreCompleto',TRUE),
                'Clave' => $this->input->post('Clave',TRUE),
                'EmailContacto' => $this->input->post('EmailContacto',TRUE),
            );
            $this->PymeUsuario_model->insert($data_user);

            $last_user_insert_id = $this->db->insert_id();

            ///////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////

            $data_pyme = array(
                'NombreComercio' => $this->input->post('NombreComercio',TRUE),
                'EstadoID' => $this->input->post('EstadoID',TRUE),
                'SectorID' => $this->input->post('SectorID',TRUE),
                'AnnoInicioOperaciones' => $this->input->post('AnnoInicioOperaciones',TRUE),
                'NumeroTelefono' => $this->input->post('NumeroTelefono',TRUE),
                'Direccion' => $this->input->post('Direccion',TRUE),
                'EsActiva' => 1,
                'EsNegocioFamiliar' => $this->input->post('EsNegocioFamiliar',TRUE),
                'Logo' => $this->input->post('Logo',TRUE),
                'ExtensionLogo' => $ext,
                'FechaCreacion' => date('Y-m-d h:i:s A'),
                'FechaUltimaActualizacion' => $this->input->post('FechaUltimaActualizacion',TRUE),
                'EsFacebookAppInstalado' => 0,
                'UsuarioID' => $last_user_insert_id,
                'GeneroPropietarioID' => $this->input->post('GeneroPropietarioID',TRUE),
                'CedJuridica' => $this->input->post('CedJuridica',TRUE),
            );
            $this->Pyme_model->insert($data_pyme);
            


            $last_pyme_insert_id = $this->db->insert_id();


            for ($i=0; $i < 5; $i++) { 

                $data_red = array(
                'InformacionContacto' => $this->input->post('InformacionContacto',TRUE),
                'Link' => $this->input->post('valor['.$i.']',TRUE),
                'TipoRedSocialID'=> $this->input->post('id['.$i.']',TRUE),
                'PymeID' => $last_pyme_insert_id
                );


                $this->PymeRedsocial_model->insert($data_red);

            }


            $this->session->set_flashdata('message', 'Create Record Success');

            ///////////////////////////////////////////////////////

            redirect(site_url('login'));
        }
    }

    public function update($id) 
    {
        $row = $this->Pyme_model->get_by_id($id);

        if ($row) {
            $data_pyme = array(
                'isCreate' => '0',
                'u_paisID' => $this->Pyme_model->get_pais_by_state($row->EstadoID),
                'u_estadoID' => $row->EstadoID,
                'u_sectorID' => $row->SectorID,
                'u_generoPropietarioID' => $row->GeneroPropietarioID,
                'u_annoInicioOperaciones' => $row->AnnoInicioOperaciones,
                'button' => 'Update',
                'action' => site_url('pyme/update_action'),
        'Id' => set_value('Id', $row->Id),
        'NombreComercio' => set_value('NombreComercio', $row->NombreComercio),
        'PaisID' => set_value('EstadoID', $this->Pyme_model->get_pais()),
        'EstadoID' => set_value('EstadoID', $this->Pyme_model->get_all_states()),
        'SectorID' => set_value('SectorID', $this->Pyme_model->get_sectors()),
        'AnnoInicioOperaciones' => set_value('AnnoInicioOperaciones', $this->Pyme_model->get_creation_year()),
        'NumeroTelefono' => set_value('NumeroTelefono', $row->NumeroTelefono),
        'Direccion' => set_value('Direccion', $row->Direccion),
        'EsActiva' => set_value('EsActiva', $row->EsActiva),
        'EsNegocioFamiliar' => set_value('EsNegocioFamiliar', $row->EsNegocioFamiliar),
        'Logo' => set_value('Logo', $row->Logo),
        'ExtensionLogo' => set_value('ExtensionLogo', $row->ExtensionLogo),
        'FechaCreacion' => set_value('FechaCreacion', $row->FechaCreacion),
        'FechaUltimaActualizacion' => set_value('FechaUltimaActualizacion', $row->FechaUltimaActualizacion),
        'EsFacebookAppInstalado' => set_value('EsFacebookAppInstalado', $row->EsFacebookAppInstalado),
        'UsuarioID' => set_value('UsuarioID', $row->UsuarioID),
        'GeneroPropietarioID' => set_value('GeneroPropietarioID', $this->Pyme_model->get_gender()),
        'CedJuridica' => set_value('CedJuridica', $row->CedJuridica),
        );
        

        $id_usuario = $row->UsuarioID;
        $id_pyme = $row->Id;

        $row = $this->PymeUsuario_model->get_by_id($id_usuario);
        $data_user = array(
                'button' => 'Update',
                'action' => site_url('pymeusuario/update_action'),
        'ID' => set_value('ID', $row->ID),
        'Usuario' => set_value('Usuario', $row->Usuario),
        'NombreCompleto' => set_value('NombreCompleto', $row->NombreCompleto),
        'Clave' => set_value('Clave', $row->Clave),
        'EmailContacto' => set_value('EmailContacto', $row->EmailContacto),
        );


        $row = $this->PymeRedsocial_model->get_by_id_pyme($id_pyme);
        $data_red = array(
                'button' => 'Update',
                'action' => site_url('pymeRedsocial/update_action'),
                'TipoRedSocialID' => $this->PymeRedsocial_model->get_red_social_type(),
        );


        foreach ($row as $key => $value) {

            $data_red["id"][$key+1] =  $value->Link; 
            $data_red["valor"][$key+1] =  $value->Link;
            $data_red["InformacionContacto"] =  $value->InformacionContacto;  
        }


        
        $data['form_pyme']  =   $this->load->view('pyme/pyme_form', $data_pyme, TRUE);
        $data['form_user']  =   $this->load->view('pymeusuario/usuario_form',$data_user, TRUE);
        $data['form_red']   =   $this->load->view('pymeredsocial/redsocial_form',$data_red, TRUE);

        
        $data['action']     =   site_url('pymeRegistrar/update_action');
        

        $this->load->view('pymeregistrar',$data);



        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pyme'));
        }
    }


    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('Id', TRUE));
        } else {
            $data_pyme = array(
        'NombreComercio' => $this->input->post('NombreComercio',TRUE),
        'EstadoID' => $this->input->post('EstadoID',TRUE),
        'SectorID' => $this->input->post('SectorID',TRUE),
        'AnnoInicioOperaciones' => $this->input->post('AnnoInicioOperaciones',TRUE),
        'NumeroTelefono' => $this->input->post('NumeroTelefono',TRUE),
        'Direccion' => $this->input->post('Direccion',TRUE),
        'EsActiva' => $this->input->post('EsActiva',TRUE),
        'EsNegocioFamiliar' => $this->input->post('EsNegocioFamiliar',TRUE),
        'Logo' => $this->input->post('Logo',TRUE),
        'ExtensionLogo' => $this->input->post('ExtensionLogo',TRUE),
        
        'FechaCreacion' => $this->input->post('FechaCreacion',TRUE),
        'FechaUltimaActualizacion' => $this->input->post('FechaUltimaActualizacion',TRUE),
        
        'EsFacebookAppInstalado' => $this->input->post('EsFacebookAppInstalado',TRUE),
        'UsuarioID' => $this->input->post('UsuarioID',TRUE),
        'GeneroPropietarioID' => $this->input->post('GeneroPropietarioID',TRUE),
        'CedJuridica' => $this->input->post('CedJuridica',TRUE),
        );

            $this->Pyme_model->update($this->input->post('Id', TRUE), $data_pyme);



            $data_user = array(
            'Usuario' => $this->input->post('Usuario',TRUE),
            'NombreCompleto' => $this->input->post('NombreCompleto',TRUE),
            'Clave' => $this->input->post('Clave',TRUE),
            'EmailContacto' => $this->input->post('EmailContacto',TRUE),
            );

            $this->PymeUsuario_model->update($this->input->post('ID', TRUE), $data_user);



            for ($i=0; $i < 5; $i++) { 

                $data_red = array(
                'InformacionContacto' => $this->input->post('InformacionContacto',TRUE),
                'Link' => $this->input->post('valor['.$i.']',TRUE),
                'TipoRedSocialID'=> $this->input->post('id['.$i.']',TRUE),
                'PymeID' => $this->input->post('Id', TRUE)
                );

                $this->PymeRedsocial_model->update_id_type(  $data_red['TipoRedSocialID'],  $data_red['PymeID'] , $data_red);
                
            }



            redirect(site_url('pyme'));
  

        }
    }


    public function _rules() 
    {
    $this->form_validation->set_rules('NombreComercio', 'nombrecomercio', 'trim|required|max_length[100]');
    $this->form_validation->set_rules('EstadoID', 'estadoid', 'trim|required');
    $this->form_validation->set_rules('SectorID', 'sectorid', 'trim|required');
    $this->form_validation->set_rules('AnnoInicioOperaciones', 'annoiniciooperaciones', 'trim|required|max_length[100]');
    $this->form_validation->set_rules('NumeroTelefono', 'numerotelefono', 'trim|required');
    $this->form_validation->set_rules('Direccion', 'direccion', 'trim|required|max_length[100]');
    /*$this->form_validation->set_rules('EsActiva', 'esactiva', 'trim|required');
    $this->form_validation->set_rules('EsNegocioFamiliar', 'esnegociofamiliar', 'trim|required');
    $this->form_validation->set_rules('Logo', 'logo', 'trim|required');
    $this->form_validation->set_rules('ExtensionLogo', 'extensionlogo', 'trim|required');
    $this->form_validation->set_rules('FechaCreacion', 'fechacreacion', 'trim|required');
    $this->form_validation->set_rules('FechaUltimaActualizacion', 'fechaultimaactualizacion', 'trim|required');
    $this->form_validation->set_rules('EsFacebookAppInstalado', 'esfacebookappinstalado', 'trim|required');
    $this->form_validation->set_rules('UsuarioID', 'usuarioid', 'trim|required');*/
    $this->form_validation->set_rules('GeneroPropietarioID', 'generopropietarioid', 'trim|required');
    //$this->form_validation->set_rules('CedJuridica', 'cedjuridica', 'trim|required');
    $this->form_validation->set_rules('Usuario', 'usuario', 'trim|required');
    $this->form_validation->set_rules('NombreCompleto', 'nombrecompleto', 'trim|required');
    $this->form_validation->set_rules('Clave', 'clave', 'trim|required');
    $this->form_validation->set_rules('EmailContacto', 'emailcontacto', 'trim|required');
    $this->form_validation->set_rules('ID', 'ID', 'trim');
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');



    $this->form_validation->set_rules('Id', 'Id', 'trim');
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }



}