<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('template.php',$output);
	}

	public function usuarios()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function usuarios_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('usuario');
			$crud->set_subject('Usuario');
			$crud->required_fields('Usuario','NombreCompleto','Clave','EmailContact');
			$crud->columns('ID','Usuario','NombreCompleto','Clave','EmailContact');
			$crud->display_as('NombreCompleto','Nombre Completo');
			$crud->display_as('EmailContact','Email');

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}