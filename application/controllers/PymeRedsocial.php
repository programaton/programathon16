<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PymeRedsocial extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
        $this->load->helper('url');

        $this->load->model('PymeRedsocial_model');
        
        $this->load->library(array('session','form_validation'));   

    }

    public function index()
    {
        $data['title'] = 'Registrar Pyme';
        $data['pyme']  =  $this->session->userdata('Pyme');
        $data['user']  =  $this->session->userdata('user');

        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pymeRedsocial/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pymeRedsocial/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pymeRedsocial/index.html';
            $config['first_url'] = base_url() . 'pymeRedsocial/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->PymeRedsocial_model->total_rows($q);
        $pymeredsocial = $this->PymeRedsocial_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pymeredsocial_data' => $pymeredsocial,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('pymeredsocial/redsocial_list', $data);
    }

    public function read($id) 
    {
        $row = $this->PymeRedsocial_model->get_by_id($id);
        if ($row) {
            $data = array(
		'TipoRedSocialID' => $row->TipoRedSocialID,
		'Comentario' => $row->Comentario,
		'InformacionContacto' => $row->InformacionContacto,
		'PymeID' => $row->PymeID,
		'Link' => $row->Link,
	    );
            $this->load->view('pymeredsocial/redsocial_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeredsocial'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pymeRedsocial/create_action'),
	    'TipoRedSocialID' => $this->PymeRedsocial_model->get_red_social_type(),
	    'Comentario' => set_value('Comentario'),
	    'InformacionContacto' => set_value('InformacionContacto'),
	    'PymeID' => set_value('PymeID'),
	    'Link' => set_value('Link'),
        'TipoRedSocialID' => $this->PymeRedsocial_model->get_red_social_type(),
        'Registrar' => TRUE,
	);
        $this->load->view('pymeredsocial/redsocial_form', $data);
    }
    
    public function create_action() 
    {

        echo "<pre>";

        print_r($this->input->post());


        $this->_rules();


//        if ($this->form_validation->run() == FALSE) {
//            $this->create();
//       } else 



            for ($i=0; $i < 5; $i++) { 

                $data = array(
                'InformacionContacto' => $this->input->post('InformacionContacto',TRUE),
                'Link' => $this->input->post('valor['.$i.']',TRUE),
                'TipoRedSocialID'=> $this->input->post('id['.$i.']',TRUE),
                'PymeID' => $this->session->userdata('pyme_id')
                );


                $this->PymeRedsocial_model->insert($data);

            }
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pymeredsocial'));


    }
    
    public function update($id) 
    {
        $row = $this->PymeRedsocial_model->get_by_id_pyme($id);

        if ($row) {

        $data = array(
                'button' => 'Update',

                'action' => site_url('pymeRedsocial/update_action'),
                'TipoRedSocialID' => $this->PymeRedsocial_model->get_red_social_type(),
                'Registrar' => true
        );


        foreach ($row as $key => $value) {

            $data["id"][$key+1] =  $value->Link; 
            $data["valor"][$key+1] =  $value->Link;
            $data["InformacionContacto"] =  $value->InformacionContacto;  
        }

            $this->load->view('pymeredsocial/redsocial_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeredsocial'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        //if ($this->form_validation->run() == FALSE) {
        //    $this->update( $this->session->userdata('pyme_id') );
        //} else {

        $_SESSION['pyme_id']    =   22;

        echo "<pre>";

        print_r($this->input->post());


            for ($i=0; $i < 5; $i++) { 

                $data = array(
                'InformacionContacto' => $this->input->post('InformacionContacto',TRUE),
                'Link' => $this->input->post('valor['.$i.']',TRUE),
                'TipoRedSocialID'=> $this->input->post('id['.$i.']',TRUE),
                'PymeID' => $this->session->userdata('pyme_id')
                );

                $this->PymeRedsocial_model->update_id_type(  $data['TipoRedSocialID'],  $data['PymeID'] , $data);
                
            }

            redirect(site_url('pymeredsocial'));
    }
    
    public function delete($id) 
    {
        $row = $this->PymeRedsocial_model->get_by_id($id);

        if ($row) {
            $this->PymeRedsocial_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pymeredsocial'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeredsocial'));
        }
    }

    public function _rules() 
    {

//	$this->form_validation->set_rules('Comentario', 'comentario', 'trim|required');
//	$this->form_validation->set_rules('InformacionContacto', 'informacioncontacto', 'trim|required');
//	$this->form_validation->set_rules('Link', 'link', 'trim|required');
//	$this->form_validation->set_rules('TipoRedSocialID', 'TipoRedSocialID', 'trim');



	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file PymeRedsocial.php */
/* Location: ./application/controllers/PymeRedsocial.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-09-24 23:31:40 */
/* http://harviacode.com */