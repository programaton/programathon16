<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PymeUsuario extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

        $this->load->model('PymeUsuario_model');
        $this->load->library(array('session','form_validation'));   

    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pymeusuario/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pymeusuario/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pymeusuario/index.html';
            $config['first_url'] = base_url() . 'pymeusuario/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->PymeUsuario_model->total_rows($q);
        $pymeusuario = $this->PymeUsuario_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pymeusuario_data' => $pymeusuario,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('pymeusuario/usuario_list', $data);
    }

    public function read($id) 
    {
        $row = $this->PymeUsuario_model->get_by_id($id);
        if ($row) {
            $data = array(
		'ID' => $row->ID,
		'Usuario' => $row->Usuario,
		'NombreCompleto' => $row->NombreCompleto,
		'Clave' => $row->Clave,
		'EmailContacto' => $row->EmailContacto,
	    );
            $this->load->view('pymeusuario/usuario_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeusuario'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pymeusuario/create_action'),
	    'ID' => set_value('ID'),
	    'Usuario' => set_value('Usuario'),
	    'NombreCompleto' => set_value('NombreCompleto'),
	    'Clave' => set_value('Clave'),
	    'EmailContacto' => set_value('EmailContacto'),
        'Registrar' => TRUE,
	);
        $this->load->view('pymeusuario/usuario_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'Usuario' => $this->input->post('Usuario',TRUE),
		'NombreCompleto' => $this->input->post('NombreCompleto',TRUE),
		'Clave' => $this->input->post('Clave',TRUE),
		'EmailContacto' => $this->input->post('EmailContacto',TRUE),
	    );

            $this->PymeUsuario_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pymeusuario'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->PymeUsuario_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pymeusuario/update_action'),
		'ID' => set_value('ID', $row->ID),
		'Usuario' => set_value('Usuario', $row->Usuario),
		'NombreCompleto' => set_value('NombreCompleto', $row->NombreCompleto),
		'Clave' => set_value('Clave', $row->Clave),
		'EmailContacto' => set_value('EmailContacto', $row->EmailContacto),
	    );
            $this->load->view('pymeusuario/usuario_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeusuario'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('ID', TRUE));
        } else {
            $data = array(
		'Usuario' => $this->input->post('Usuario',TRUE),
		'NombreCompleto' => $this->input->post('NombreCompleto',TRUE),
		'Clave' => $this->input->post('Clave',TRUE),
		'EmailContacto' => $this->input->post('EmailContacto',TRUE),
	    );

            $this->PymeUsuario_model->update($this->input->post('ID', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pymeusuario'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->PymeUsuario_model->get_by_id($id);

        if ($row) {
            $this->PymeUsuario_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pymeusuario'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pymeusuario'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('Usuario', 'usuario', 'trim|required');
	$this->form_validation->set_rules('NombreCompleto', 'nombrecompleto', 'trim|required');
	$this->form_validation->set_rules('Clave', 'clave', 'trim|required');
	$this->form_validation->set_rules('EmailContacto', 'emailcontacto', 'trim|required');

	$this->form_validation->set_rules('ID', 'ID', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file PymeUsuario.php */
/* Location: ./application/controllers/PymeUsuario.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-09-24 23:31:07 */
/* http://harviacode.com */