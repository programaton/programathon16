<?php 
class Pymes extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->database();
    $this->load->helper('url');
    $this->load->library('grocery_CRUD');
    $this->load->model('Pymes_Model');
  }


  public function _pymes_output($output = null)
  {
    $this->load->view('pymes.php',$output);
  }

  public function index()
  {
    $this->load->view('welcome_message');
  }

  public function index_pymes_management()
  {
    try{
      $crud = new grocery_CRUD();

      $crud->set_theme('datatables');
      $crud->set_table('pyme');
      $crud->set_subject('pyme');
      $crud->required_fields('id');
      $crud->columns('id');


      $crud->field_type('EstadoID', 'dropdown', $this->Pymes_Model->get_all_states());


      $crud->set_relation('estadoId', 'estado', 'id');

      
      $output = $crud->render();

      $this->_pymes_output($output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }

}