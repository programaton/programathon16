<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pyme extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    
        $this->load->database();
	    $this->load->helper('url');

        $this->load->model('Pyme_model');
        $this->load->library(array('session','form_validation'));   

    }

    public function index()
    {

        $data['title'] = 'Lista de Pyme';
        $data['pyme']  =  $this->session->userdata('Pyme');
        $data['user']  =  $this->session->userdata('user');



        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pyme/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pyme/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pyme/index.html';
            $config['first_url'] = base_url() . 'pyme/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pyme_model->total_rows($q);
        $pyme = $this->Pyme_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pyme_data' => $pyme,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('pyme/pyme_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Pyme_model->get_by_id($id);
        if ($row) {
            $data = array(
		'Id' => $row->Id,
		'NombreComercio' => $row->NombreComercio,
		'EstadoID' => $row->EstadoID,
		'SectorID' => $row->SectorID,
		'AnnoInicioOperaciones' => $row->AnnoInicioOperaciones,
		'NumeroTelefono' => $row->NumeroTelefono,
		'Direccion' => $row->Direccion,
		'EsActiva' => $row->EsActiva,
		'EsNegocioFamiliar' => $row->EsNegocioFamiliar,
		'Logo' => $row->Logo,
		'ExtensionLogo' => $row->ExtensionLogo,
		'FechaCreacion' => $row->FechaCreacion,
		'FechaUltimaActualizacion' => $row->FechaUltimaActualizacion,
		'EsFacebookAppInstalado' => $row->EsFacebookAppInstalado,
		'UsuarioID' => $row->UsuarioID,
		'GeneroPropietarioID' => $row->GeneroPropietarioID,
		'CedJuridica' => $row->CedJuridica,
	    );
            $this->load->view('pyme/pyme_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pyme'));
        }
    }

    public function create() 
    {
        $data = array(
            'isCreate' => '1',
            'button' => 'Create',
            'action' => site_url('pyme/create_action'),
        'Id' => set_value('Id'),
        'NombreComercio' => set_value('NombreComercio'),
        'PaisID' => $this->Pyme_model->get_pais(),
        'EstadoID' => $this->Pyme_model->get_all_states(),
        'SectorID' => $this->Pyme_model->get_sectors(),
        'AnnoInicioOperaciones' => $this->Pyme_model->get_creation_year(),
        'NumeroTelefono' => set_value('NumeroTelefono'),
        'Direccion' => set_value('Direccion'),
        'EsActiva' => set_value('EsActiva'),
        'EsNegocioFamiliar' => set_value('EsNegocioFamiliar'),
        'Logo' => set_value('Logo'),
        'ExtensionLogo' => set_value('ExtensionLogo'),
        'FechaCreacion' => set_value('FechaCreacion'),
        'FechaUltimaActualizacion' => set_value('FechaUltimaActualizacion'),
        'EsFacebookAppInstalado' => set_value('EsFacebookAppInstalado'),
        'UsuarioID' => set_value('UsuarioID'),
        'GeneroPropietarioID' => $this->Pyme_model->get_gender(),
        'CedJuridica' => set_value('CedJuridica'),
        'Registrar' => TRUE,
    );
        $this->load->view('pyme/pyme_form', $data);
    }
    
    public function create_action() 
    {	
    	$path = $this->input->post('Logo',TRUE);
    	$ext  =	'';

    	if( !empty($path) )
    	{
    		$ext = pathinfo($path, PATHINFO_EXTENSION);
    	}
        
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'NombreComercio' => $this->input->post('NombreComercio',TRUE),
		'EstadoID' => $this->input->post('EstadoID',TRUE),
		'SectorID' => $this->input->post('SectorID',TRUE),
		'AnnoInicioOperaciones' => $this->input->post('AnnoInicioOperaciones',TRUE),
		'NumeroTelefono' => $this->input->post('NumeroTelefono',TRUE),
		'Direccion' => $this->input->post('Direccion',TRUE),
		'EsActiva' => 0,
		'EsNegocioFamiliar' => $this->input->post('EsNegocioFamiliar',TRUE),
		'Logo' => $this->input->post('Logo',TRUE),
		'ExtensionLogo' => $ext,
		'FechaCreacion' => date('l jS \of F Y h:i:s A'),
		'FechaUltimaActualizacion' => $this->input->post('FechaUltimaActualizacion',TRUE),
		'EsFacebookAppInstalado' => 0,
		'UsuarioID' => $this->session->userdata('user_id'),
		'GeneroPropietarioID' => $this->input->post('GeneroPropietarioID',TRUE),
		'CedJuridica' => $this->input->post('CedJuridica',TRUE),
	    );

            $this->Pyme_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pyme'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pyme_model->get_by_id($id);

        if ($row) {
            $data = array(
                'isCreate' => '0',
                'u_paisID' => $this->Pyme_model->get_pais_by_state($row->EstadoID),
                'u_estadoID' => $row->EstadoID,
                'u_sectorID' => $row->SectorID,
                'u_generoPropietarioID' => $row->GeneroPropietarioID,
                'u_annoInicioOperaciones' => $row->AnnoInicioOperaciones,
                'button' => 'Update',
                'action' => site_url('pyme/update_action'),
        'Id' => set_value('Id', $row->Id),
        'NombreComercio' => set_value('NombreComercio', $row->NombreComercio),
        'PaisID' => set_value('EstadoID', $this->Pyme_model->get_pais()),
        'EstadoID' => set_value('EstadoID', $this->Pyme_model->get_all_states()),
        'SectorID' => set_value('SectorID', $this->Pyme_model->get_sectors()),
        'AnnoInicioOperaciones' => set_value('AnnoInicioOperaciones', $this->Pyme_model->get_creation_year()),
        'NumeroTelefono' => set_value('NumeroTelefono', $row->NumeroTelefono),
        'Direccion' => set_value('Direccion', $row->Direccion),
        'EsActiva' => set_value('EsActiva', $row->EsActiva),
        'EsNegocioFamiliar' => set_value('EsNegocioFamiliar', $row->EsNegocioFamiliar),
        'Logo' => set_value('Logo', $row->Logo),
        'ExtensionLogo' => set_value('ExtensionLogo', $row->ExtensionLogo),
        'FechaCreacion' => set_value('FechaCreacion', $row->FechaCreacion),
        'FechaUltimaActualizacion' => set_value('FechaUltimaActualizacion', $row->FechaUltimaActualizacion),
        'EsFacebookAppInstalado' => set_value('EsFacebookAppInstalado', $row->EsFacebookAppInstalado),
        'UsuarioID' => set_value('UsuarioID', $row->UsuarioID),
        'GeneroPropietarioID' => set_value('GeneroPropietarioID', $this->Pyme_model->get_gender()),
        'CedJuridica' => set_value('CedJuridica', $row->CedJuridica),
        );
            $this->load->view('pyme/pyme_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pyme'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('Id', TRUE));
        } else {
            $data = array(
		'NombreComercio' => $this->input->post('NombreComercio',TRUE),
		'EstadoID' => $this->input->post('EstadoID',TRUE),
		'SectorID' => $this->input->post('SectorID',TRUE),
		'AnnoInicioOperaciones' => $this->input->post('AnnoInicioOperaciones',TRUE),
		'NumeroTelefono' => $this->input->post('NumeroTelefono',TRUE),
		'Direccion' => $this->input->post('Direccion',TRUE),
		'EsActiva' => $this->input->post('EsActiva',TRUE),
		'EsNegocioFamiliar' => $this->input->post('EsNegocioFamiliar',TRUE),
		'Logo' => $this->input->post('Logo',TRUE),
		'ExtensionLogo' => $this->input->post('ExtensionLogo',TRUE),
		'FechaCreacion' => $this->input->post('FechaCreacion',TRUE),
		'FechaUltimaActualizacion' => $this->input->post('FechaUltimaActualizacion',TRUE),
		'EsFacebookAppInstalado' => $this->input->post('EsFacebookAppInstalado',TRUE),
		'UsuarioID' => $this->session->userdata('user_id'),
		'GeneroPropietarioID' => $this->input->post('GeneroPropietarioID',TRUE),
		'CedJuridica' => $this->input->post('CedJuridica',TRUE),
	    );

            $this->Pyme_model->update($this->input->post('Id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pyme'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pyme_model->get_by_id($id);

        if ($row) {
            $this->Pyme_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pyme'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pyme'));
        }
    }

    public function _rules() 
    {
	/*$this->form_validation->set_rules('NombreComercio', 'nombrecomercio', 'trim|required');*/
	$this->form_validation->set_rules('EstadoID', 'estadoid', 'trim|required');
	$this->form_validation->set_rules('SectorID', 'sectorid', 'trim|required');
	/*$this->form_validation->set_rules('AnnoInicioOperaciones', 'annoiniciooperaciones', 'trim|required');
	$this->form_validation->set_rules('NumeroTelefono', 'numerotelefono', 'trim|required');
	$this->form_validation->set_rules('Direccion', 'direccion', 'trim|required');
	$this->form_validation->set_rules('EsActiva', 'esactiva', 'trim|required');
	$this->form_validation->set_rules('EsNegocioFamiliar', 'esnegociofamiliar', 'trim|required');
	$this->form_validation->set_rules('Logo', 'logo', 'trim|required');
	$this->form_validation->set_rules('ExtensionLogo', 'extensionlogo', 'trim|required');
	$this->form_validation->set_rules('FechaCreacion', 'fechacreacion', 'trim|required');
	$this->form_validation->set_rules('FechaUltimaActualizacion', 'fechaultimaactualizacion', 'trim|required');
	$this->form_validation->set_rules('EsFacebookAppInstalado', 'esfacebookappinstalado', 'trim|required');
	$this->form_validation->set_rules('UsuarioID', 'usuarioid', 'trim|required');*/
	$this->form_validation->set_rules('GeneroPropietarioID', 'generopropietarioid', 'trim|required');
	//$this->form_validation->set_rules('CedJuridica', 'cedjuridica', 'trim|required');

	$this->form_validation->set_rules('Id', 'Id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
