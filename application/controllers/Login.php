<?php 

class Login extends CI_Controller{
   
   function __construct() 
   { 
        parent::__construct();
        $this ->load->model('Login_model');
        $this->load->library(array('session','form_validation'));   
        $this->load->library('grocery_CRUD');
           
   }
  
function index()
    { 


        $_SESSION['user_id']    =   NULL;
        $_SESSION['estado_id']  =   NULL;
        $_SESSION['pyme_id']    =   NULL;

        if (  1  ==  $this->session->userdata('logued_in') ) { 

            $data['title']      = 'Administrador';
            $data['pyme']       =  $this->session->userdata('Pyme');
            $data['user']       =  $this->session->userdata('user');

            $output = "<h1>Bienvenido " . $this->session->userdata('user') . "</h1>" ;

            $data['output'] =  $output;
           
            $this->load->view('template.php', $data);

            return true;

        }

       
    
    // reglas de validación
       $this->form_validation->set_rules('username', 'Usuario', 'callback__valid_login');
       $this->form_validation->set_rules('pymename', 'Pymename', 'callback__valid_pyme');
       $this->form_validation->set_rules('pais', 'Pais', 'callback__valid_pais');
    
      
        //$this->form_validation->set_message('required', 'el campo %s es requerido');
        $this->form_validation->set_message('_valid_login', 'El usuario o contraseña son incorrectos');
        $this->form_validation->set_message('_valid_pyme', 'Por Favor verifique su nombre de comercio');
        $this->form_validation->set_message('_valid_pais', 'Por Favor verifique que el pais seleccionado sea el correcto o no este vacio');
      
        $this -> form_validation -> set_error_delimiters('<ul><li>', '</li></ul>');
    
        if ($this->form_validation->run() == FALSE)
        {
            $data['title'] = 'Programathon - Sistema de Login';
            $data['paises'] = $this->Login_model->get_all_paises();

            $this->load->view('admin/login',$data);
        }
        else{
                         
             $username = $this->input->post('username');
             $pymename = $this->input->post('pymename');
             
             $data_user = $array = array('user'      => $username
                                        ,'logued_in' => TRUE
                                        ,'pyme'      => $pymename
                                        ,'Pyme_id'   => $_SESSION['pyme_id']
                                        ,);
             
            // asignamos dos datos a la sesión --> (username y logued_in)                                     
            $this->session->set_userdata($data_user); 
            
            $data['title'] = 'Administrador'; 
            $data['user'] = $username;  // = $this->session->userdata('user');
            $data['pyme']       =  $this->session->userdata('Pyme');
           
            $output = "<h1>Bienvenido " . $this->session->userdata('user') . "</h1>" ;

            $data['output'] =  $output;

            //$this->load->view('dashboard.php',$data);
			redirect(site_url('panel-de-metricas'),$data);
                 
               
        }
  }
   
function _valid_login($username,$password)
{ 
        $username = $this->input->post('username');
        //$password = md5($this->input->post('password'));
        $password = $this->input->post('password');
        $pymename = $this->input->post('pymename');
        $pais = $this->input->post('pais');


        $resultUser = $this->Login_model->valid_user($username,$password);
    
        if (TRUE ==  $resultUser['STATUS']) {
            $_SESSION['user_id']    =   $resultUser['USER_ID']; 
        }

        return $resultUser['STATUS'];
        
}


function _valid_pyme()
{       
        $username = $this->input->post('username');
        $pymename = $this->input->post('pymename');

        if (!is_null( $_SESSION['user_id'] ) ) {
           
            $validatePyme = $this->Login_model->valid_pyme($pymename, $_SESSION['user_id']);


            if (TRUE ==  $validatePyme['STATUS']) { 
                $_SESSION['estado_id']    =   $validatePyme['ESTADO_ID']; 
                $_SESSION['pyme_id']      =   $validatePyme['PYME_ID']; 
                
                return $validatePyme['STATUS'];
            }

            return $validatePyme;
        }

        return FALSE;       
}

function _valid_pais()
{ 
        $username = $this->input->post('username');
        $pais = $this->input->post('pais');

        if('' == $pais)
        {
            return false;
        }
        else
        {

            if (!is_null( $_SESSION['estado_id'] ) ) {

                $validatePais = $this->Login_model->valid_pais($pais, $_SESSION['estado_id']); 

                return $validatePais;

            }
        }

        
        
}


       

function valid_login_ajax(){
    //verificamos si la petición es via ajax
if($this->input->is_ajax_request()){

         if($this->input->post('username')!==''){
            $username = $this->input->post('username');
             
        $this->Login_model->valid_user_ajax($username);   

        }
}else{
        redirect('login');
}

} // fin del método valid_login_ajax
   
function logout(){
             
         $this->session->sess_destroy(); 
         redirect('login');       
}
  
}