<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model{

public function __construct()
{
  // Call the CI_Model constructor
  parent::__construct();
  $this->load->database();

}

function valid_user($username, $password)
{

  $this->db->where('Usuario', $username);
  $this->db->where('Clave', $password);
  
  $query = $this->db->get('usuario');

  $result = array('STATUS' => FALSE);

  if($query->num_rows() >0)
  {

    $userData = $query->result();
    $userid = $userData[0]->ID;

    $result['STATUS']   = TRUE;
    $result['USER_ID']  = $userid;

    return $result;
   
    }else{
       
        return $result;
    }
}


function valid_pyme($pyme, $userid)
{

    $this->db->where('UsuarioID', $userid);
    $this->db->where('NombreComercio', $pyme);

    $query = $this->db->get('pyme');

    $result = array('STATUS' => FALSE);

    if($query->num_rows() >0)
    {

      $pymeData = $query->result();
      $estadoId = $pymeData[0]->EstadoID;
      $pymeId = $pymeData[0]->Id;

      $result['STATUS']   = TRUE;
      $result['ESTADO_ID']  = $estadoId;
      $result['PYME_ID']  = $pymeId;

      return $result;
    }
    else
    {
      return $result['STATUS'];
    }


}


function valid_pais($pais, $estadoid)
{

    $this->db->where('Id', $estadoid);
    $this->db->where('PaisID', $pais);

    $query = $this->db->get('estado');

    if($query->num_rows() >0)
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }

}




function valid_user_ajax($username){ 
           
    die("TEST");
    $this->db->where('Usuario', $username);
    $query = $this->db->get('usuario');
         
         if($query->num_rows() >0){
               
             echo $query->num_rows();
             
             }
  }



function get_all_paises()
{
    $query = $this->db->get('pais');
    $paises = array('0' => '');

    foreach ($query->result() as $pais) 
    {
      $paises[$pais->Id] = $pais->Nombre;
    }

    return $paises;

}

 
}